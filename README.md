# Lingo24 PHP Code Sniffer Standard

A Lingo24 code sniffer standard based on a relaxed version of PSR-2.

## Usage

This project can be included as a Composer development dependency and the referenced in a `phpcs` ruleset.

```
#!json
{
    "repositories": [
        {
           "type": "vcs",
           "url": "git@bitbucket.org:lingo24/phpcs-standard.git"
        }
    ],

    "require-dev": {
        "lingo24/phpcs-standard": "*",
    },
}
```

```
#!xml
<?xml version="1.0"?>
<ruleset name="Coach">
    ...
    <rule ref="vendor/lingo24/phpcs-standard/Lingo24" />
    ...
</ruleset>
```