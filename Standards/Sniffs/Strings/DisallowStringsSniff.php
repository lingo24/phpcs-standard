<?php
/**
 * This sniff prohibits the use of Perl style hash comments.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   PHP_CodeSniffer
 */

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

/**
 * This sniff prohibits the use of Perl style hash comments.
 *
 * An example of a hash comment is:
 *
 * <code>
 *  # This is a hash comment, which is prohibited.
 *  $hello = 'hello';
 * </code>
 *
 * @category  PHP
 * @package   PHP_CodeSniffer
 */
class Standards_Sniffs_Strings_DisallowStringsSniff implements Sniff {

    public function register() {
        return array(T_STRING);
    }

    public function process(File $phpcsFile, $stackPtr) {
        $tokens = $phpcsFile->getTokens();

        $error = 'found %s';
        $data  = array(trim($tokens[$stackPtr]['content']));
        if ($tokens[$stackPtr]['content'] == 'QFirebug' && $tokens[$stackPtr + 1]['content'] == '::') {
            $phpcsFile->addError($error, $stackPtr, 'Found', $data, true);
        }
    }
}
